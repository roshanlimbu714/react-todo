import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyAjDuvM7UCJPT00imvs5kskIE8LkFjVtrU",
    authDomain: "bim-task-management.firebaseapp.com",
    databaseURL: "https://bim-task-management-default-rtdb.asia-southeast1.firebasedatabase.app",
    projectId: "bim-task-management",
    storageBucket: "bim-task-management.appspot.com",
    messagingSenderId: "577433579735",
    appId: "1:577433579735:web:dd9ac3da9a5e60e1cbc092",
    measurementId: "G-7GCKWPWJBW"
};

const fire = !firebase.apps.length ? firebase.initializeApp(firebaseConfig) : null;


export default fire;