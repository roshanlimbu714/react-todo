import * as actionTypes from './types';


export const increaseCounter = val => {
    return {
        type: actionTypes.INCREASE_COUNT,
        payload: val
    }
}


export const decreaseCounter = val => {
    return {
        type: actionTypes.DECREASE_COUNT,
        payload: val
    }
}