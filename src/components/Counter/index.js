import {Navbar} from "../Common/Navbar";
import {useDispatch, useSelector} from "react-redux";
import {increaseCounter, decreaseCounter} from "./actions";

export const Counter = props => {
    const dispatch = useDispatch();

    const count = useSelector(state => state['counterReducer'].count);
    return <>
        <Navbar/>
        <div className={'flex'}>
            <button className={'btn primary'}
                    onClick={() => dispatch(increaseCounter(count + 1))}>
                Increase
            </button>
            <div className="title"> Count: {count}</div>
            <button className={'btn primary'} onClick={() => dispatch(increaseCounter(count - 1))}>Decrease</button>
        </div>
    </>
}