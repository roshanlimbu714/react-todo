import {useDispatch, useSelector} from "react-redux";
import {useState} from "react";
import {validateForm} from "../../util/validate";
import {login} from "./store/actions";
import {useHistory} from "react-router";

const initState = {email: '', password: ''};

export const Login = (props) => {
    const dispatch = useDispatch();
    const history = useHistory();
    const [creds, setCreds] = useState({email: 'admin@task.com', password: 'password'});
    const [errors, setErrors] = useState({...initState});
    const changeHandler = e => {
        setCreds({...creds, [e.target.name]: e.target.value})
    }


    const validationRules = {
        email: {
            label: 'Email',
            required: true,
        },
        password: {
            label: 'Password',
            required: true,
        }
    };
    const userLogin =async  e => {
        e.preventDefault();
        let err = validateForm(validationRules, creds);
        setErrors(err);
        if (Object.keys(err).length === 0) {
            try {
                dispatch(login(creds));
                history.push('/dashboard');
            } catch (e) {
                console.log(e);
            }
        }
    }
    return (
        <section className="fullscreen flex-centered">
            <div className="card">
                <div className="login-title title mb-lg">Login</div>
                <form className="form-area" onSubmit={userLogin}>
                    <div className="form-group mb-sm">
                        <label>Email</label>
                        <input type="text" className={''} onChange={changeHandler} value={creds.email} placeholder={''}
                               name='email'/>
                        <span className={'text-danger'}>{errors.email}</span>
                    </div>
                    <div className="form-group ">
                        <label>Password</label>
                        <input type={'password'} className={''} value={creds.password} onChange={changeHandler}
                               placeholder={''} name={'password'}/>
                        <span className={'text-danger'}>{errors.password}</span>

                    </div>
                    <div className="button-area flex mt-xl">
                        <button className="btn primary full-width">Login</button>
                    </div>
                </form>
            </div>
        </section>
    )
}