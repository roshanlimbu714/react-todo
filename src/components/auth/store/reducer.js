import * as actionTypes from './types.js';
import {TODO} from "../../../util/constants";

const initialState = {
    user:{},
    isAuthenticated: false
}

export const AuthReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case actionTypes.SET_USER:
            return {
                ...state,
                user: actions.payload,
                isAuthenticated: true
            }
        case actionTypes.LOGOUT:
            return {
                ...state,
                user: null,
                isAuthenticated: false
            }
        default:
            return state
    }
}