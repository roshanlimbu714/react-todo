import * as actionTypes from './types';
import fire from "../../../plugins/firebase";

export const setUser = user => {
    return {
        type: actionTypes.SET_USER,
        payload: user
    }
}
export const login = ({email, password}) => dispatch => {
    console.log(email,password);
    fire.auth().signInWithEmailAndPassword(email, password).then(res => {
        console.log(res);
        dispatch(setUser(res));
    });
}