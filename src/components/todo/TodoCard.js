import {useEffect, useState} from "react";

export const TodoCard = ({task,changeTaskStatus,removeTask, editTask}) => {
    const categories = ['Open', 'In Progress', 'Review', 'Done'];
    const [taskDisplayed, setTaskDisplayed] = useState(task);

    useEffect(()=>{
        setTaskDisplayed(task);
    }, [task]);

    useEffect(()=>{
        changeTaskStatus(taskDisplayed);
    }, [taskDisplayed]);


    const changeStatusOfTaskDisplayed = e =>{
        setTaskDisplayed({
            ...taskDisplayed,
            status: e.target.value
        });

    }
    return (
        <div className="todo-card card">
            <div className="close-btn" onClick={removeTask}>X</div>
            <div className="edit-btn" onClick={editTask}>E</div>
            <span className="todo-title">
                {taskDisplayed.title}
            </span>
            <span className="todo-description">
                {taskDisplayed.description}
            </span>
            <div className="form-group mt-md">
                <select name="category" value={taskDisplayed.status} onChange={changeStatusOfTaskDisplayed}>
                    {categories.map((v, key) => <option key={key} value={v}>{v}</option>)}
                </select>
            </div>
        </div>
    );
}