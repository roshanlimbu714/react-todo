import {useEffect, useState} from "react";
import {validateForm} from "../../util/validate";
import {useDispatch} from "react-redux";

const initState = {title: '', description: ''};

export const AddTodoForm = props => {
    const dispatch = useDispatch();
    const [task, setTask] = useState({...initState});
    const [mode, setMode] = useState('Add');
    const validationRules = {
        title: {
            label: 'Title',
            required: true,
        },
        description: {
            label: 'Description',
            required: true,
        }
    };

    useEffect(()=>{
       if(props.taskSelected){
           setMode('Edit');
           setTask({...props.taskSelected});
       }else{
           setMode('Add');
       }
        console.log(mode);
    },[]);

    const [errors, setErrors] = useState({...initState});

    const onChangeHandler = e => setTask({
        ...task, [e.target.name]: e.target.value
    });

    const addTask = (e) => {
        e.preventDefault();
        let err = validateForm(validationRules,task);
        setErrors(err);
        if(Object.keys(err).length === 0){
            try{
                if(mode==='Edit'){
                    props.editConfirm({...task})
                }else{
                    props.addTask({
                        ...task,
                        status:'Open'
                    });
                }
                props.hide();
            }catch(e){
                console.log(e);
            }
        }
        setMode('Add');
    }


    return (
        <form className={'add-todo'} onSubmit={addTask}>
            <div className="title">{mode} task</div>
            <div className="form-group mt-lg">
                <label>Title</label>
                <input type="text" placeholder={'Enter title'} value={task.title} name={'title'} onChange={onChangeHandler}/>
                <span className={'text-danger'}>{errors.title}</span>
            </div>
            <div className="form-group mt-md">
                <label>Description</label>
                <input type="text" placeholder={'Enter description'} value={task.description} name={'description'} onChange={onChangeHandler}/>
                <span className={'text-danger'}>{errors.description}</span>

            </div>
            <div className="mt-md">
                <button className={'btn primary full-width'} type={'submit'}>Add</button>
            </div>
        </form>
    )
}