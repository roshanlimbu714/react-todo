import {Navbar} from "../Common/Navbar";
import {useEffect, useState} from "react";
import {TodoArea} from "./tabs/TodoArea";
import {Modal} from "../Common/Modal";
import {AddTodoForm} from "./AddTodoForm";
import {useDispatch, useSelector} from "react-redux";
import {addTask, editTask, deleteTask, loadTasks, changeStatus} from "./store/actions";

export const Todo = (props) => {
    const dispatch = useDispatch();
    const tasks = useSelector(state=> state['TodoReducer'].tasks)
    const [active, setActive] = useState(0);
    const [isAppear, setIsAppear] = useState(false);
    const tabs = ['Overview', 'Tasks', 'Completed'];
    const changeActive = (val) => setActive(val);
    const [taskSelected, setSelectedTask] = useState(null);

    useEffect(()=>{
        loadTasksList();
    },[]);

    const showModal = () => setIsAppear(true);
    const hideModal = () => {
        setIsAppear(false);
        setSelectedTask({});
    };

    const changeCardStatus = (task) => {
        dispatch(changeStatus(task));
    }

    const loadTasksList =async  (val) => {
        await dispatch(loadTasks());
    }

    const addToForm = async (val) => {
        dispatch(addTask(val));
        await dispatch(loadTasks());
    }

    const removeTask = (id) => {
        dispatch(deleteTask(id))
    }

    const showEditDialog = (val) => {
        setSelectedTask(val);
        showModal();
    }

    const editConfirm =async val => {
        dispatch(editTask(val));
        await dispatch(loadTasks());
        setSelectedTask(null);
    }
    return (
        <>
            <Navbar/>
            <section className={'wrapper-x py-lg'}>
                <div className="flex justify-between">
                    <div className="title">Tasks Board</div>
                    <button className="btn primary" onClick={showModal}>
                        Add Task
                    </button>
                </div>

                <div className="tab">
                    <div className="tab-menu flex">
                        {
                            tabs.map((v, key) =>
                                <div className={`tab-menu-item pa-md ${active === key ? 'bg-primary text-light' : ''}`}
                                     onClick={() => changeActive(key)} key={key}>{v}
                                </div>)
                        }
                    </div>

                    <div className="tab-content py-lg">
                        <TodoArea editDialog={showEditDialog} tasks={tasks} changeStatus={changeCardStatus}
                                  removeTask={removeTask}/>
                    </div>
                </div>
            </section>
            {isAppear && <Modal hide={hideModal}>
                <AddTodoForm addTask={addToForm} hide={hideModal} taskSelected={taskSelected}
                             editConfirm={editConfirm}/>
            </Modal>}
        </>
    )
}