import * as actionTypes from './types';
import {TODO} from "../../../util/constants";
import store from "../../../store/store";
import fire from '../../../plugins/firebase';

export const setTask = (val) => ({
    type: actionTypes.SET_TASK,
    payload: val
});

export const loadTasks = () => dispatch => {
    fire.database().ref('/todo').on('value', snapshot => {
        const tasks = Object.values(snapshot.val()).map((v, key) => ({...v, id: Object.keys(snapshot.val())[key]}));
        dispatch(setTask(tasks ?? TODO));
    });
};
export const addTask = (val) => dispatch => {
    fire.database().ref('/todo').push(val);
    const storeList = store.getState().TodoReducer.tasks;
    dispatch(setTask([...storeList, val]));
};

export const editTask = (val) => dispatch => {
    fire.database().ref('/todo/' + val.id).set(val).then(v => {
        console.log(v)
    });
};

export const deleteTask = (id) => dispatch => {
    fire.database().ref('/todo/' + id).remove();
};


export const changeStatus = (task) => dispatch => {
    fire.database().ref('/todo/' + task.id).set(task).then(v => {
    });
};
