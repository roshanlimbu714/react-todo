import * as actionTypes from './types.js';
import {TODO} from "../../../util/constants";

const initialState = {
    tasks: [],
    selectedTask: {}
}

export const TodoReducer = (state = initialState, actions) => {
    switch (actions.type) {
        case actionTypes.LOAD_TASKS:
            return {
                ...state
            }
        case actionTypes.SET_TASK:
            return {
                ...state,
                tasks: actions.payload
            }
        default:
            return state
    }
}