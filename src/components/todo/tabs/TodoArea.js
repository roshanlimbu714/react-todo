import {TodoCard} from "../TodoCard";
import {useEffect} from "react";
import {useSelector} from "react-redux";

export const TodoArea = props => {
    const tasks = useSelector(state=> state['TodoReducer'].tasks)

    const categories = ['Open', 'In Progress', 'Review', 'Done'];
    return (
        <div className="todo-tab flex">
            {categories.map((cat,key) => (
                <div className={'flex-1 todo-column'} key={key}>
                    <div className="title">{cat}</div>
                    <div className="tasks-list">
                        {tasks && tasks.map((v, key) =>
                            v.status === cat && (
                                <TodoCard
                                    editTask={()=>props.editDialog(v)}
                                    removeTask={()=>props.removeTask(v.id)}
                                    key={key}
                                    task={v}
                                    changeTaskStatus={props.changeStatus}/>
                            ))}
                    </div>
                </div>
            ))}
        </div>
    );
}