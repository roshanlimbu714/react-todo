export const Navbar = props => {
    return (
        <nav className={'full-width flex items-center justify-between'}>
            <div className="logo">Logo</div>
            <div className="avatar">JD</div>
        </nav>
    )
};