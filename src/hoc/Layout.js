import {Routes} from "../routes";

export const Layout = ()=> {
    return (
        <main>
            <Routes/>
        </main>
    )
}