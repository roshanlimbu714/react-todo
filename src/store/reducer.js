import {combineReducers} from "redux";
import {counterReducer} from "../components/Counter/reducer";
import {TodoReducer} from "../components/todo/store/reducer";
import {AuthReducer} from "../components/auth/store/reducer";

export default combineReducers({
    counterReducer,
    TodoReducer,
    AuthReducer
});