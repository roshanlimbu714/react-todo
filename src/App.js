import logo from './logo.svg';
import './App.scss';
import {Layout} from "./hoc/Layout";

function App() {
  return (
    <div className="App">
       <Layout/>
    </div>
  );
}

export default App;
