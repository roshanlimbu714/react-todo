export const validateForm = (rules, data) => {
    let message = {};
    Object.keys(rules).forEach(key => {
        if (rules[key]?.required && data[key] === '') {
            message = {
                ...message,
                [key]: rules[key].label + ' is required.'
            };
        }
    });

    return message;
}