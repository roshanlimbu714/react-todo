export const TODO =  [
    {
        id: 1,
        title: 'Todo 1',
        description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci blanditiis
                cumque,
                deserunt eaque error hic illum, in iste magni minima mollitia neque, provident rerum sequi similique vel
                vitae.`,
        status: 'Open',
    }, {
        id: 2,
        title: 'Todo 2',
        description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci blanditiis
                cumque,
                deserunt eaque error hic illum, in iste magni minima mollitia neque, provident rerum sequi similique vel
                vitae.`,
        status: 'Open',
    }, {
        id: 3,
        title: 'Todo 3',
        description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci blanditiis
                cumque,
                deserunt eaque error hic illum, in iste magni minima mollitia neque, provident rerum sequi similique vel
                vitae.`,
        status: 'In Progress',
    }, {
        id: 4,
        title: 'Todo 4',
        description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci blanditiis
                cumque,
                deserunt eaque error hic illum, in iste magni minima mollitia neque, provident rerum sequi similique vel
                vitae.`,
        status: 'Open',
    }, {
        id: 5,
        title: 'Todo 5',
        description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci blanditiis
                cumque,
                deserunt eaque error hic illum, in iste magni minima mollitia neque, provident rerum sequi similique vel
                vitae.`,
        status: 'Review',
    }, {
        id: 6,
        title: 'Todo 6',
        description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci blanditiis
                cumque,
                deserunt eaque error hic illum, in iste magni minima mollitia neque, provident rerum sequi similique vel
                vitae.`,
        status: 'Open',
    }, {
        id: 7,
        title: 'Todo 7',
        description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci blanditiis
                cumque,
                deserunt eaque error hic illum, in iste magni minima mollitia neque, provident rerum sequi similique vel
                vitae.`,
        status: 'Open',
    }, {
        id: 8,
        title: 'Todo 8',
        description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci blanditiis
                cumque,
                deserunt eaque error hic illum, in iste magni minima mollitia neque, provident rerum sequi similique vel
                vitae.`,
        status: 'Done',
    }, {
        id: 9,
        title: 'Todo 9',
        description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci blanditiis
                cumque,
                deserunt eaque error hic illum, in iste magni minima mollitia neque, provident rerum sequi similique vel
                vitae.`,
        status: 'In Progress',
    }, {
        id: 10,
        title: 'Todo 10',
        description: `Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus accusantium adipisci blanditiis
                cumque,
                deserunt eaque error hic illum, in iste magni minima mollitia neque, provident rerum sequi similique vel
                vitae.`,
        status: 'Done',
    },
];