import {Login} from "../components/auth/Login";
import {Todo} from "../components/todo/Todo";

export default [
    {
        path:'/dashboard',
        component: Todo,
        exact:true
    },{
        path:'',
        component: Login,
        exact:true
    },
]
